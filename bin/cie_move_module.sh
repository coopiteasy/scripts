#!/bin/bash

set -e

PROG=$(basename "$0")

usage_text="Usage: ${PROG} MODULE SOURCE_REPOSITORY TARGET_REPOSITORY"

help_text="${usage_text}

  Moves module MODULE from SOURCE_REPOSITORY to TARGET_REPOSITORY
  - MODULE is the module name (python module, aka technical name)
  - SOURCE_REPOSITORY and TARGET_REPOSITORY are url
  - these urls must be accessible through https or ssh (git protocol)

  The module will be removed on branch 12.0-rem-MODULE from source repository.
  The commit history will be pushed on branch 12.0-add-MODULE of the target repository.

  These branches are pushed on the repositories but the PR will not be opened
  by this script

Options:
  -h  show this help text and exit."

while getopts ':h' option; do
    case "$option" in
        h)
            echo "${help_text}"
            exit 0
            ;;
        \?)
            printf "Error: illegal option: -%s\n" "${OPTARG}" >&2
            echo "${help_text}" >&2
            exit 1
            ;;
    esac
done
shift $((OPTIND - 1))

if [ $# -ne 3 ]; then
    echo "Error: $0 takes 3 positional argument, but $# given." >&2
    echo "${usage_text}"
    exit 1
fi

MODULE=$1               # module name
SOURCE_REPOSITORY=$2    # path to source repository
TARGET_REPOSITORY=$3    # path to target repository
ODOO_VERSION="12.0"

PATCH_NAME=$MODULE.patch
TARGET_REPOSITORY_NAME=$(basename $TARGET_REPOSITORY)

# create patch from source repository
cd $SOURCE_REPOSITORY || exit
git checkout $ODOO_VERSION -b $ODOO_VERSION-rem-$MODULE
git format-patch --keep-subject --stdout "$(git rev-list --max-parents=0 HEAD)" -- $MODULE > $TARGET_REPOSITORY/$PATCH_NAME
rm -rf $MODULE
git commit -am "[REM] $MODULE: move to $TARGET_REPOSITORY_NAME"
git push coopiteasy

# apply patch in target repository
cd $TARGET_REPOSITORY || exit
git checkout $ODOO_VERSION -b $ODOO_VERSION-add-$MODULE
git am -3 --keep $PATCH_NAME
rm $PATCH_NAME
oca-gen-addon-readme --repo-name $TARGET_REPOSITORY_NAME --branch $ODOO_VERSION --org-name coopiteasy --addon-dir $MODULE
git commit -am "[IMP] generate readme"
git push coopiteasy
