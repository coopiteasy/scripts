#!/usr/bin/env python3

# Copyright 2020 Coop IT Easy SCRLfs (http://coopiteasy.be)
# License GPL-3.0 or later (https://www.gnu.org/licenses/gpl.html).


import csv
import sys
import base64
import os
from glob import glob
import argparse
import logging


def create_parser():
    """Return parser for this script."""
    parser = argparse.ArgumentParser(
        prog="base64_conversion",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="""
Make a copy of a csv file, replacing the image filename with a base64 representation of the image.
Use it to import image into Odoo.
        """,
    )

    parser.add_argument(
        "-i",
        "--input",
        required=True,
        help="Input csv file",
    )
    parser.add_argument(
        "-p",
        "--path",
        required=True,
        help="Path to the folder containing the image files",
    )
    parser.add_argument(
        "-o",
        "--output",
        default="output.csv",
        help="Destination file",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        default=0,
        help="""Show debug information. Specify multiple time to
        increase debug level.""",
    )
    return parser


def run():
    """Parse args, configure and run the program"""
    args = vars(create_parser().parse_args())
    # use vars to get args as dict instead of namespace
    logging.basicConfig(
        format="%(asctime)s %(levelname)s %(message)s",
        level=logging.ERROR - args["verbose"] * 10,
    )
    main(args)


def main(args):
    """Program starts here."""
    logging.info("Starting conversion")
    csv.field_size_limit(sys.maxsize)
    # name of the columns fields:
    # first field is for the main image, second field is for the extra images
    IMAGE_COLS = ("image_medium", "product_image_ids/image")

    with open(args["input"], "r") as idfile:
        ids = csv.DictReader(idfile)

        with open(args["output"], "w") as output_file:
            output = csv.DictWriter(
                output_file, fieldnames=ids.fieldnames, quoting=csv.QUOTE_ALL
            )
            output.writeheader()

            for row in ids:
                for image_col in IMAGE_COLS:
                    if not row[image_col]:
                        logging.debug("Empty image cell")
                        continue
                    image_path = os.path.join(args["path"], row[image_col])
                    logging.debug("Converting image %s" % image_path)
                    if not os.path.exists(image_path):
                        file_list = glob(args["path"] + "/*")
                        file_match = [
                            file for file in file_list if image_path in file
                        ]
                        if not file_match:
                            logging.warning(
                                "Image path %s not found, adding empty field"
                                % image_path
                            )
                            row[image_col] = ""
                            continue
                        else:
                            image_path = file_match[0]
                            if len(file_match) > 1:
                                logging.warning(
                                    "More than one corresponding image file found, only kept the first one"
                                )
                    with open(image_path, "rb") as image_file:
                        encoded_string = base64.b64encode(
                            image_file.read()
                        ).decode("ascii")
                        # Decoding because the base64 encoding produces a binary string, with is then
                        # surrounded by quotes in the output csv file. Decoding now prevents the "b'"
                        # prefix to be added to the string

                    row[image_col] = encoded_string
                output.writerow(row)


if __name__ == "__main__":
    run()
