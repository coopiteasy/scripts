# Copyright 2020 Coop IT Easy SCRLfs (http://coopiteasy.be)
# License GPL-3.0 or later (https://www.gnu.org/licenses/gpl.html).

"""Test for cleanbu script"""

import importlib
import re
import tempfile

from datetime import datetime, timedelta
from math import ceil
from pathlib import Path


__bindir__ = Path(__file__).parent.parent.parent / "bin"
__testdir__ = Path(__file__).parent

# Load cleanbu functions
# As cleanbu is not a standard python package (no .py extension), this
# trick must be used to import his content.
# pylint: disable=invalid-name
cleanbuspec = importlib.util.spec_from_loader(
    "cleanbu",
    importlib.machinery.SourceFileLoader(
        "cleanbu", str(__bindir__ / "cleanbu")
    ),
)
cleanbu = importlib.util.module_from_spec(cleanbuspec)
cleanbuspec.loader.exec_module(cleanbu)
# pylint: enable=invalid-name


def setup_testfile_default_filter(directory):
    """
    Create a fake backup twice a day during the past 6 years and
    return a set of these files.
    """
    listofpath = set()
    buname = "templatebu"
    now = datetime.now()
    for date in (now - timedelta(days=n) for n in range(ceil(6 * 365.25))):
        filepattern = "{name}-{date}"
        middaybu = filepattern.format(
            name=buname,
            date=date.replace(hour=12, minute=5).strftime("%Y-%m-%d-%H-%M"),
        )
        nightbu = filepattern.format(
            name=buname,
            date=date.replace(hour=0, minute=5).strftime("%Y-%m-%d-%H-%M"),
        )
        listofpath.add(directory / middaybu)
        (directory / middaybu).touch()
        listofpath.add(directory / nightbu)
        (directory / nightbu).touch()
    # Add a random file
    listofpath.add(directory / "random")
    return listofpath


def test_keepall_filter():
    """Test for the keepall filter"""
    with tempfile.TemporaryDirectory() as tmpdir:
        tmpdir = Path(tmpdir)
        files = setup_testfile_default_filter(tmpdir)
        keep, delete, ignore = cleanbu.keepall_filter(files)
        assert keep == files
        assert delete == set()
        assert ignore == set()


def test_default_filter():
    """Test for the default filter"""
    bkfmt = re.compile(cleanbu.BKFMT)
    with tempfile.TemporaryDirectory() as tmpdir:
        tmpdir = Path(tmpdir)
        files = setup_testfile_default_filter(tmpdir)
        keep, delete, ignore = cleanbu.default_filter(files, bkfmt, debug=True)
        # Print file that are kept sorted to control it by a human
        for file in sorted(keep):
            print(file)
        # Check returned sets
        assert len(keep) == len(files) - len(delete) - len(ignore)
        assert len(delete) == len(files) - len(keep) - len(ignore)
        assert len(ignore) == len(files) - len(keep) - len(delete)
        assert len(keep & delete) == 0
        assert len(keep & ignore) == 0
        assert len(delete & ignore) == 0
        assert len(keep) != len(keep) + len(delete) + len(ignore)
        assert len(ignore) == 1


def test_onemonth_filter():
    """Test for the onemonth filter"""
    bkfmt = re.compile(cleanbu.BKFMT)
    with tempfile.TemporaryDirectory() as tmpdir:
        tmpdir = Path(tmpdir)
        files = setup_testfile_default_filter(tmpdir)
        keep, delete, ignore = cleanbu.onemonth_filter(
            files, bkfmt, debug=True
        )
        # Print file that are kept sorted to control it by a human
        for file in sorted(keep):
            print(file)
        # Check returned sets
        assert len(keep) == len(files) - len(delete) - len(ignore)
        assert len(delete) == len(files) - len(keep) - len(ignore)
        assert len(ignore) == len(files) - len(keep) - len(delete)
        assert len(keep & delete) == 0
        assert len(keep & ignore) == 0
        assert len(delete & ignore) == 0
        assert len(keep) != len(keep) + len(delete) + len(ignore)
        assert len(ignore) == 1


def test_threemonth_filter():
    """Test for the threemonth filter"""
    bkfmt = re.compile(cleanbu.BKFMT)
    with tempfile.TemporaryDirectory() as tmpdir:
        tmpdir = Path(tmpdir)
        files = setup_testfile_default_filter(tmpdir)
        keep, delete, ignore = cleanbu.threemonth_filter(
            files, bkfmt, debug=True
        )
        # Print file that are kept sorted to control it by a human
        for file in sorted(keep):
            print(file)
        # Check returned sets
        assert len(keep) == len(files) - len(delete) - len(ignore)
        assert len(delete) == len(files) - len(keep) - len(ignore)
        assert len(ignore) == len(files) - len(keep) - len(delete)
        assert len(keep & delete) == 0
        assert len(keep & ignore) == 0
        assert len(delete & ignore) == 0
        assert len(keep) != len(keep) + len(delete) + len(ignore)
        assert len(ignore) == 1


def test_oneweek_filter():
    """Test for the oneweek filter"""
    bkfmt = re.compile(cleanbu.BKFMT)
    with tempfile.TemporaryDirectory() as tmpdir:
        tmpdir = Path(tmpdir)
        files = setup_testfile_default_filter(tmpdir)
        keep, delete, ignore = cleanbu.oneweek_filter(files, bkfmt, debug=True)
        # Print file that are kept sorted to control it by a human
        for file in sorted(keep):
            print(file)
        # Check returned sets
        assert len(keep) == len(files) - len(delete) - len(ignore)
        assert len(delete) == len(files) - len(keep) - len(ignore)
        assert len(ignore) == len(files) - len(keep) - len(delete)
        assert len(keep & delete) == 0
        assert len(keep & ignore) == 0
        assert len(delete & ignore) == 0
        assert len(keep) != len(keep) + len(delete) + len(ignore)
        assert len(ignore) == 1


def test_twodays_filter():
    """Test for the oneweek filter"""
    bkfmt = re.compile(cleanbu.BKFMT)
    with tempfile.TemporaryDirectory() as tmpdir:
        tmpdir = Path(tmpdir)
        files = setup_testfile_default_filter(tmpdir)
        keep, delete, ignore = cleanbu.twodays_filter(files, bkfmt, debug=True)
        # Print file that are kept sorted to control it by a human
        for file in sorted(keep):
            print(file)
        # Check returned sets
        assert len(keep) == len(files) - len(delete) - len(ignore)
        assert len(delete) == len(files) - len(keep) - len(ignore)
        assert len(ignore) == len(files) - len(keep) - len(delete)
        assert len(keep & delete) == 0
        assert len(keep & ignore) == 0
        assert len(delete & ignore) == 0
        assert len(keep) != len(keep) + len(delete) + len(ignore)
        assert len(ignore) == 1
