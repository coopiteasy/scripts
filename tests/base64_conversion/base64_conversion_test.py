# Copyright 2021 Coop IT Easy SCRLfs (http://coopiteasy.be)
# License GPL-3.0 or later (https://www.gnu.org/licenses/gpl.html).

"""Test for base64_conversion script"""

from csv import DictReader
from pathlib import Path
import importlib
import os
import unittest
import csv
import sys

unittest.TestLoader.sortTestMethodsUsing = None


class TestBase64Conversion(unittest.TestCase):
    """Base class for testing base64 conversion"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.__bindir__ = Path(__file__).parent.parent.parent / "bin"
        self.__testdir__ = Path(__file__).parent
        self.input_path = os.path.join(self.__testdir__, "data", "test.csv")
        self.output_path = os.path.join(self.__testdir__, "data", "output.csv")
        self.args = {
            "input": self.input_path,
            "path": os.path.join(self.__testdir__, "images"),
            "output": self.output_path,
        }

    def setUp(self):
        base64_conversion_spec = importlib.util.spec_from_loader(
            "base64_conversion",
            importlib.machinery.SourceFileLoader(
                "base64_conversion",
                str(self.__bindir__ / "base64_conversion"),
            ),
        )
        base64_conversion = importlib.util.module_from_spec(
            base64_conversion_spec
        )
        base64_conversion_spec.loader.exec_module(base64_conversion)

        csv.field_size_limit(sys.maxsize)
        base64_conversion.main(self.args)

    def tearDown(self):
        if os.path.exists(self.output_path):
            os.remove(self.output_path)


class TestBase64ConversionWithoutDictReader(TestBase64Conversion):
    """Run tests before converting data with DictReader"""

    def test_output_file_exists(self):
        """Tests that output file exists"""
        self.assertTrue(os.path.exists(self.output_path))


class TestBase64ConversionWithDictReader(TestBase64Conversion):
    """Run tests after converting data with DictReader"""

    def setUp(self):
        super().setUp()
        with open(self.input_path, "rt", encoding="utf8") as input_file:
            with open(self.output_path, "rt", encoding="utf8") as output_file:
                self.input_dict = list(DictReader(input_file))
                self.output_dict = list(DictReader(output_file))
                # return a list to be able to close the file

    def test_same_size_input_output(self):
        """Test that input and output has the same size"""
        self.assertEqual(len(self.input_dict), len(self.output_dict))

    def test_base64_for_existing_file_with_extension(self):
        """Test conversion to base64 for existing file with extension"""
        id_to_test = "__export__.product_template_35875_868948a1"
        base64_to_test = next(
            item["image_medium"]
            for item in self.output_dict
            if item["id"] == id_to_test
        )
        # Comparing beggining and end of base64 string (easier than
        # saving the whole string into a file and reading it)
        base64_reference_start = "/9j/4QLaRXhpZgAASUkqAAgAAAAGAA4BAgAXAAAAVg"
        base64_reference_end = "qCuRVsYoaGKHN0OKWm64Vf/9D/2Q=="
        self.assertEqual(base64_reference_start, base64_to_test[:42])
        self.assertEqual(base64_reference_end, base64_to_test[-30:])

    def test_base64_for_image_extra_without_extension(self):
        """Test conversion to base64 for existing file without extension"""
        image_name_to_test = "Product Image extra"
        base64_reference_start = "/9j/4AAQSkZJRgABAgEAyADIAAD/4VZaRXhpZgAASU"
        base64_reference_end = "V6GG2f+OH5M9Wn8EfQ+aK7TI//2Q=="
        base64_to_test = next(
            item["product_image_ids/image"]
            for item in self.output_dict
            if item["product_image_ids_extra/name"] == image_name_to_test
        )
        self.assertEqual(base64_reference_start, base64_to_test[:42])
        self.assertEqual(base64_reference_end, base64_to_test[-30:])

    def test_missing_image_file(self):
        """Test when image file is missing"""
        id_to_test = "__export__.product_template_35203_3a194a2d"
        base64_to_test = next(
            item["image_medium"]
            for item in self.output_dict
            if item["id"] == id_to_test
        )
        self.assertEqual(base64_to_test, "")
