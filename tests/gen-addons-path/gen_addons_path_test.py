# Copyright 2023 Coop IT Easy SC (http://coopiteasy.be)
# License GPL-3.0 or later (https://www.gnu.org/licenses/gpl.html).

"""Test for gen-addons-path script"""

import subprocess

from pathlib import Path


__bindir__ = Path(__file__).parent.parent.parent / "bin"
__testdir__ = Path(__file__).parent

ADDONS_PATH = "odoo/odoo/addons,odoo/addons,coopiteasy/addons,oca/addons"
ADDONS_PATH_WITH_SRC = (
    "src/odoo/odoo/addons,src/odoo/addons,src/coopiteasy/addons,src/oca/addons"
)
ADDONS_PATH_FULL = ",".join(
    [
        str(__testdir__ / "data/src/odoo/odoo/addons"),
        str(__testdir__ / "data/src/odoo/addons"),
        str(__testdir__ / "data/src/coopiteasy/addons"),
        str(__testdir__ / "data/src/oca/addons"),
    ]
)
ADDONS_PATH_VALID_FULL = ",".join(
    [
        str(__testdir__ / "data/src/odoo/odoo/addons"),
        str(__testdir__ / "data/src/odoo/addons"),
        str(__testdir__ / "data/src/coopiteasy/addons"),
    ]
)


def run(args):
    """Execute main program with args"""
    return subprocess.run(
        [str(__bindir__ / "gen-addons-path")] + args,
        capture_output=True,
        encoding="utf-8",
        check=True,
    )


def test_valid_dotted():
    """Test for a valid dotted conf file"""
    test_repo_path = __testdir__ / "data/valid-dotted.yml"
    process = run([str(test_repo_path)])
    assert process.stdout.strip() == ADDONS_PATH


def test_valid_ssh():
    """Test for a valid ssh conf file"""
    test_repo_path = __testdir__ / "data/valid-ssh.yml"
    process = run([str(test_repo_path)])
    assert process.stdout.strip() == ADDONS_PATH


def test_valid_http():
    """Test for a valid http conf file"""
    test_repo_path = __testdir__ / "data/valid-http.yml"
    process = run([str(test_repo_path)])
    assert process.stdout.strip() == ADDONS_PATH


def test_valid_git_dotted():
    """Test for a valid git dotted conf file"""
    test_repo_path = __testdir__ / "data/valid-git-dotted.yml"
    process = run([str(test_repo_path)])
    assert process.stdout.strip() == ADDONS_PATH


def test_valid_http_with_dir():
    """Test for a valid http conf file"""
    test_repo_path = __testdir__ / "data/valid-http.yml"
    process = run(["--base-directory", "src", str(test_repo_path)])
    assert process.stdout.strip() == ADDONS_PATH_WITH_SRC


def test_with_no_extra_key():
    """Test getting addons path without extra key 'is_odoo_core'"""
    test_repo_path = __testdir__ / "data/valid-no-extra-key.yml"
    process = run(
        [
            "--base-directory",
            __testdir__ / "data/src",
            str(test_repo_path),
        ]
    )
    assert process.stdout.strip() == ADDONS_PATH_FULL


def test_only_valid():
    """Test getting only valid addons path"""
    test_repo_path = __testdir__ / "data/valid-http.yml"
    process = run(
        [
            "--base-directory",
            __testdir__ / "data/src",
            "--only-valid",
            str(test_repo_path),
        ]
    )
    assert process.stdout.strip() == ADDONS_PATH_VALID_FULL


def test_only_valid_not_existing():
    """Test getting only valid addons path with a repo that does not exists"""
    test_repo_path = __testdir__ / "data/valid-not-existing-dir.yml"
    process = run(
        [
            "--base-directory",
            __testdir__ / "data/src",
            "--only-valid",
            str(test_repo_path),
        ]
    )
    assert process.stdout.strip() == ADDONS_PATH_VALID_FULL


def test_only_valid_with_module():
    """Test getting only valid addons path with a repo that point to a
    module and not a list of module
    """
    test_repo_path = __testdir__ / "data/valid-with-module.yml"
    process = run(
        [
            "--base-directory",
            __testdir__ / "data/src",
            "--only-valid",
            str(test_repo_path),
        ]
    )
    assert process.stdout.strip() == ADDONS_PATH_VALID_FULL
