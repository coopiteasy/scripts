Scripts
=======

This repository contains scripts used in Coop IT Easy SC
(http://coopiteasy.be).

Script should be placed in `bin` directory. Script can be written in any
languages. It's would be nice if usage of this scripts does not need a
complicated installation. So keep in mind that it should not depends on
external dependencies, except the ones that comes with the language. For
example in python, try to use standard library instead of module from
PyPI. If external dependencies is really mandatory then add your
dependencies installation in this documentation.

These script will be available on server, but you can also use it on
your local computer. So feel free, to add scripts that you used
everyday.

Tests should stand in the `tests` directory. Create one subfolder for
each script.



Installation
------------

I'm not sure for the right way to install these scripts properly. But
for now, I suggest the following method. Do the following as root.

```sh
apt install python3-yaml
mkdir -p /usr/share/cie
git clone https://gitlab.com/coopiteasy/scripts /usr/share/cie/scripts
ln -s /usr/share/cie/scripts/bin /usr/ciebin
```

Then `/usr/ciebin` can be added to the default PATH. Or use the script
like this:

```sh
/usr/ciebin/<myscript>
```

To add `/usr/ciebin` to the PATH add the following line in `.bashrc`:
```sh
export PATH=/usr/ciebin:$PATH
```



Update
------

To update do the following command:

```sh
cd /usr/share/cie/scripts
git pull
```



Uninstall
---------

To remove do the following command:

```sh
rm -rf /usr/share/cie/scripts
rm -rf /usr/ciebin
```
